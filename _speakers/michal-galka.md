---
name: "Micha\u0142 Ga\u0142ka"
talks:
- Vodka powered by Whiskeyberry PI
---

Michał is a software engineer who made friends with Python 10 years ago. He's also keen on embedded systems. He tries to combine those two by spending time on MicroPython powered chips as well as SBCs like RaspberryPi. Apart from that he's a Linux and open source enthusiast and a very unprofessional bass player. Likes talking to people.
