---
name: Michael (Stu) Stewart
talks:
- Gradient Descent, Demystified
---

Stu (Michael Stewart) is a machine learning engineer at Opendoor in San Francisco. Previously, he was a data scientist and engineer at Uber, and an economic researcher at the Federal Reserve Bank of New York. He is a National Science Foundation Graduate Research Fellowship Awardee in Economic Sciences.
https://www.linkedin.com/in/mstewart141/