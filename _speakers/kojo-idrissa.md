---
name: Kojo Idrissa
talks:
- A Junior Developer's Guide to Software Engineering
---

Kojo Idrissa *was* an accountant who got an MBA and taught at university in two different countries. He's *now* a software engineer and is still trying to grow into the role. He's spoken at tech conferences about spreadsheets, contributing to tech communities, Dungeons & Dragons, inclusion and privilege. He's helped organize DjangoCon since 2016 and you MIGHT have seen him talking about [#NorAMGT](https://twitter.com/hashtag/NorAmGT?src=hash). You can find him online at http://kojoidrissa.com/ or as [@transition](https://twitter.com/Transition) on Twitter.