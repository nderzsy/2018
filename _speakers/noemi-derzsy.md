---
name: Noemi Derzsy
talks:
- Network/Graph Analysis in Python
---

Holding a PhD in Physics and research background in Network Science and Computer Science, my interests revolve around the study of complex systems and complex networks through real-world data. Currently, I am a Data Science Fellow at Insight and a NASA Datanaut. Previously, I was a postdoctoral research associate at Social Cognitive Networks Academic Research Center at Rensselaer Polytechnic Institute.