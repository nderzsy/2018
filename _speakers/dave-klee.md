---
name: Dave Klee
talks:
- "How to get started with Text Analysis in Python (and analyzing Donald Trump\u2019s
  tweets)."
---

Dave Klee is based in New York City and has spoken internationally on a variety of topics related to technology (but never Python). Over the last few years he's taken a personal tour of NLP and text classification from term counts to Naive Bayes to Random Forests and Gradient Boosting, and he is excited to help others get started solving their own text classification and processing problems with Python. He has an M.Sc. in Information Management, and yeah, is mildly obsessed with @realdonaldtrump's tweets.