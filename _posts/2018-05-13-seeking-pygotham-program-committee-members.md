---
title: Now Seeking PyGotham Program Committee Members
date: 2018-05-13T12:11-0400
attribution: https://late.am/post/2018/05/13/seeking-pygotham-program-committee-members.html
excerpt_separator: <!-- more -->
---

Last year’s PyGotham had a great program committee of four organizers and
eight volunteers. Unfortunately due to the constrained timeline of the 2017
conference and everyone’s individual schedules, we had only a few chances to
meet, and never as the full group. The major consequence of this is that the
final talk decisions were effectively made by a smaller group, mostly
conference staff. We failed, by way of logistics, in our mission to make
PyGotham as community focused as possible. This year we aim to do better.

<!-- more -->

![Conference badges](/static/images/badges.jpg){:style="max-width:100%"}
_[marc thiele](https://www.flickr.com/photos/marcthiele/)_


## Changes to the Program Process

As a guiding principal for programming the conference, we want the
organizers to act more like coaches than players in selecting the talks. We
retain the right to change the lineup, but we want most of the calls to be
made by the volunteer committee members.

To that end, this year we’re going to experiment with a different process
than in previous years. We will form sub-committees from among the
volunteers, and these subcommittees will each program a mini conference of
talks about a particular topic area. The final topic breakdown will be
guided by the set of talks we get before the close of our CFP later this
month, but we expect committees on Data Science & Machine Learning, Web
Programming, Testing, and so on.

Each subcommittee will be given a sub-set of talks from the total list as a
suggested starting point. These talks will be initially ordered by the rank
order of [public votes](/post/2017/07/20/pygotham-voting-open.html) (2018
talk voting will open a few days after the CFP closes), but picking the top
N won’t necessarily make the best conference.  Each subcommittee should feel
free to nominate talks from other topic areas’ lists if there’s a strong
reason to (or if we have made a mistake in categorization).

The job of the subcommittee will be to apply a human touch and to arrange
the talks, to the extent possible, into “mini-tracks” of 2 or 3 talks. We
will in all likelihood pick a few more talks for each subcommittee than can
actually fit into the final conference — we expect there to be some cases of
speakers being selected more than once, and we will need some speakers to be
on a wait-list in case anyone drops out between now and October. Still, it
is my hope that we can arrange these mini tracks into the final PyGotham
schedule more or less as-is.

All of the review by the subcommittees will be anonymous, so after all the
committees have produced their mini-tracks, the organizers will de-anonymize
the talk list to ensure that we haven’t selected multiple talks by a single
speaker.


## Getting Involved

Here’s where we need help from you: so far, the program committee doesn’t
exist! If you think you’ll have a few hours to spare over the first half of
June, and you want to help shape PyGotham, then we want to hear from you.
Drop us a line at [program@pygotham.org](mailto:program@pygotham.org), and
let us know if there’s a particular topic area you’re interested in helping
to review. Thanks in advance for your help! 

